import numpy as np 
import pandas as pd
from pandas import Series, DataFrame

import math
import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('whitegrid')

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn import metrics
from sklearn.preprocessing import StandartScaler
import chess.pgn

pgn = open("ficsgamesdb_2015_standard_nomovetimes_1507389.pgn")
total_data = pd.DataFrame([[0,0,0,0,0]]) 
target = pd.DataFrame([[0]])  

def ecoToId(x):
	return uniqueECO[uniqueECO['eco']== x].index.values.astype(int)[0]  

for i in range(1,20000):
    game = chess.pgn.read_game(pgn)

    bRating = game.headers["BlackElo"]
    wRating = game.headers["WhiteElo"]
    if int(bRating)>1300 or int(wRating)>1300:
        continue
    finalRating = int(wRating) -int(bRating)
    
    ecoId = game.headers["ECO"]
    winStr = game.headers["Result"]
    
    if winStr =='0-1':
        winInt = 0
        
    if winStr =='1-0':
        winInt = 1
        
    if winStr =='1/2-1/2':
        winInt = 2 
    bRook = 0
    wRook = 0
    bQueen = 0
    wQueen = 0
    bKnight = 0
    wKnight = 0
    bOfficer = 0
    wOfficer = 0
    bPawn = 0
    wPawn = 0
    bIsolatedPawn = 0
    wIsolatedPawn = 0
    bDoublePawn = 0
    wDoublePawn = 0
    bBlockedPawn = 0
    wBlockedPawn = 0

    bPiecesUnicodeSymbols = [chess.Piece(4,chess.BLACK).unicode_symbol(),chess.Piece(3,chess.BLACK).unicode_symbol(),chess.Piece(2,chess.BLACK).unicode_symbol(),chess.Piece(5,chess.BLACK).unicode_symbol(),chess.Piece(6,chess.BLACK).unicode_symbol()]
    wPiecesUnicodeSymbols = [chess.Piece(4,chess.WHITE).unicode_symbol(),chess.Piece(3,chess.WHITE).unicode_symbol(),chess.Piece(2,chess.WHITE).unicode_symbol(),chess.Piece(5,chess.WHITE).unicode_symbol(),chess.Piece(6,chess.WHITE).unicode_symbol()]
   
    board = game.board()
    for i in chess.SQUARES:
        if board.piece_at(i) is not None:
            if board.piece_at(i).unicode_symbol() == chess.Piece(4,chess.WHITE).unicode_symbol():
                wRook = wRook + 1
            elif board.piece_at(i).unicode_symbol() == chess.Piece(4,chess.BLACK).unicode_symbol():
                bRook = bRook + 1
            elif board.piece_at(i).unicode_symbol() == chess.Piece(3,chess.WHITE).unicode_symbol():
                wOfficer = wOfficer + 1	
            elif board.piece_at(i).unicode_symbol() == chess.Piece(3,chess.BLACK).unicode_symbol():
                bOfficer = bOfficer + 1		
            elif board.piece_at(i).unicode_symbol() == chess.Piece(5,chess.WHITE).unicode_symbol():	
                wQueen = wQueen + 1	
            elif board.piece_at(i).unicode_symbol() == chess.Piece(5,chess.BLACK).unicode_symbol():
                bQueen = bQueen + 1		
            elif board.piece_at(i).unicode_symbol() == chess.Piece(2,chess.WHITE).unicode_symbol():
                wKnight = wKnight + 1		
            elif board.piece_at(i).unicode_symbol() == chess.Piece(2,chess.BLACK).unicode_symbol():
                bKnight = bKnight + 1		
            elif board.piece_at(i).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                wPawn = wPawn + 1
                #blocked pawns
                if i < 56 :
                    if board.piece_at(i+8) is not None:
                        if board.piece_at(i+8).unicode_symbol() in bPiecesUnicodeSymbols :
                            wBlockedPawn = wBlockedPawn + 1
				
				 # Check for doubled pawns on a single file
                PawnColumn = i % 8
                nrPawn = 0
                for j in range(0,8):
                    if board.piece_at(PawnColumn + (j * 8)) == 	chess.Piece(1,chess.WHITE).unicode_symbol():
                        nrPawn = nrPawn + 1
                if nrPawn > 1:
                    wDoublePawn = wDoublePawn + 1

				# Check for isolated pawns
                nrPawn = 0
                PawnRow = int(i // 8)
                if PawnColumn != 0:
                    if board.piece_at(i-1) is not None:
                        if board.piece_at(i-1).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7:
                    if board.piece_at(i+1) is not None:
                        if board.piece_at(i+1).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnRow != 0:
                    if board.piece_at(i-8) is not None:
                        if board.piece_at(i-8).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7:
                    if board.piece_at(i+8) is not None:
                        if board.piece_at(i+8).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 0 and PawnRow != 0:
                    if board.piece_at(i-9) is not None:
                        if board.piece_at(i-9).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7 and PawnRow != 7:
                    if board.piece_at(i+9) is not None:
                        if board.piece_at(i+9).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 0 and PawnRow != 7:
                    if board.piece_at(i-7) is not None:
                        if board.piece_at(i-7).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7 and PawnRow != 0:
                    if board.piece_at(i+7) is not None:
                        if board.piece_at(i+7).unicode_symbol() == chess.Piece(1,chess.WHITE).unicode_symbol():
                            nrPawn = nrPawn + 1
                if nrPawn == 0:
                    wIsolatedPawn = wIsolatedPawn + 1		
                elif board.piece_at(i).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                    bPawn = bPawn + 1
                    if i > 7 :
                        if board.piece_at(i-8) is not None:
                            if board.piece_at(i-8).unicode_symbol() in wPiecesUnicodeSymbols :
                                bBlockedPawn = bBlockedPawn + 1
                PawnColumn = i % 8
                nrPawn = 0
                for j in range(0,8):
                    if board.piece_at(PawnColumn + (j * 8)) == 	chess.Piece(1,chess.BLACK).unicode_symbol():
                        nrPawn = nrPawn + 1
                if nrPawn > 1:
                    bDoublePawn = bDoublePawn + 1

				# Check for isolated pawns
                nrPawn = 0
                PawnRow = int(i // 8)
                if PawnColumn != 0:
                    if board.piece_at(i-1) is not None:
                        if board.piece_at(i-1).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7:
                    if board.piece_at(i+1) is not None:
                        if board.piece_at(i+1).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnRow != 0:
                    if board.piece_at(i-8) is not None:
                        if board.piece_at(i-8).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7:
                    if board.piece_at(i+8) is not None:
                        if board.piece_at(i+8).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 0 and PawnRow != 0:
                    if board.piece_at(i-9) is not None:
                        if board.piece_at(i-9).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7 and PawnRow != 7:
                    if board.piece_at(i+9) is not None:
                        if board.piece_at(i+9).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 0 and PawnRow != 7:
                    if board.piece_at(i-7) is not None:
                        if board.piece_at(i-7).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if PawnColumn != 7 and PawnRow != 0:
                    if board.piece_at(i+7) is not None:
                        if board.piece_at(i+7).unicode_symbol() == chess.Piece(1,chess.BLACK).unicode_symbol():
                            nrPawn = nrPawn + 1
                if nrPawn == 0:
                    bIsolatedPawn = bIsolatedPawn + 1

    MaterialCount = (wQueen-bQueen) + (wRook-bRook) + ((wOfficer-bOfficer)+(wKnight-bKnight)) + (wPawn-bPawn)
    # Mobility
    board.turn = chess.WHITE
    wMobility = len(board.legal_moves)
    board.turn = chess.BLACK
    bMobility = len(board.legal_moves)

    MobilityCount = (wMobility - bMobility)
    # Pawn Structure
    PawnStructure = (wDoublePawn - bDoublePawn) + (wIsolatedPawn - bIsolatedPawn) + (wBlockedPawn - bBlockedPawn)
	# Centre Control
    CentralControl = (len(board.attackers(chess.WHITE,35)) - len(board.attackers(chess.BLACK,35))) + (len(board.attackers(chess.WHITE,36)) - len(board.attackers(chess.BLACK,36))) + (len(board.attackers(chess.WHITE,27)) - len(board.attackers(chess.BLACK,27))) + (len(board.attackers(chess.WHITE,28)) - len(board.attackers(chess.BLACK,28))) + (len(board.attackers(chess.WHITE,19)) - len(board.attackers(chess.BLACK,19))) + (len(board.attackers(chess.WHITE,20)) - len(board.attackers(chess.BLACK,20))) + (len(board.attackers(chess.WHITE,29)) - len(board.attackers(chess.BLACK,29))) + (len(board.attackers(chess.WHITE,37)) - len(board.attackers(chess.BLACK,37))) + (len(board.attackers(chess.WHITE,43)) - len(board.attackers(chess.BLACK,43))) + (len(board.attackers(chess.WHITE,44)) - len(board.attackers(chess.BLACK,44))) + (len(board.attackers(chess.WHITE,34)) - len(board.attackers(chess.BLACK,34))) + (len(board.attackers(chess.WHITE,26)) - len(board.attackers(chess.BLACK,26)))
	
#  if winInt !=2 :
#       total_data=total_data.append([[finalRating,ecoId,MaterialCount,MobilityCount,CentralControl]], ignore_index=True)
#       target=target.append([[winInt]], ignore_index=True)
    
    total_data=total_data.append([[finalRating,ecoId,MaterialCount,MobilityCount,CentralControl]], ignore_index=True)
    target=target.append([[winInt]], ignore_index=True)
    
    
total_data.columns=['Final Rating','GameOpening Identifier','Material Count','Mobility Count','CentralControl']
#target.columns=['Winer 0-Black 1-White']
#total_data.columns=['Final Rating','Rated Game','Opening Identifier','Material Count']
total_data = total_data.iloc[1:]
target = target.iloc[1:] 
 

uniqueECO = total_data['GameOpening Identifier'].unique()
uniqueECO = pd.DataFrame({'eco':uniqueECO.tolist()})
ecoId = total_data['GameOpening Identifier'].apply(ecoToId)

total_data = total_data.drop('GameOpening Identifier', axis = 1)
total_data = pd.concat([total_data,ecoId],axis =1)

#print (total_data)
#print (target)



for i in range(1,10):
    X_train, X_test, Y_train, Y_test = train_test_split(total_data, target, test_size = 0.5)
    scaler = StandartScaler()
    scaler.fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)
    regr = MLPClassifier(solver='sgd', alpha=1e-5, hidden_layer_sizes=(12,8,5), random_state=1,max_iter=1000)
    regr.fit(X_train, np.ravel(Y_train))

    print (regr.score(X_test, np.ravel(Y_test), sample_weight=None))